const CryptoJS = require('crypto-js')
const axios = require('axios')

module.exports = {

    create: function(key, secret, demo=true) {
        return {
            key: key,
            secret: secret,
            host: demo ? 'https://test.finerymarkets.com/api/' : 'https://trade.finerymarkets.com/api/',

            request: async function (method, content) {
                let payload = JSON.stringify({
                    ...content,
                    "nonce": new Date().valueOf(),
                    "timestamp": new Date().valueOf()
                })

                let signature = CryptoJS.HmacSHA384(method + payload, this.secret).toString(CryptoJS.enc.Base64)

                const response = await axios({
                    method: 'POST',
                    url: this.host + method,
                    headers: {
                        'EFX-Key': this.key,
                        'EFX-Sign': signature,
                        'Content-Type': 'text/html'
                    },
                    data: payload
                })
                return response.data
            }
        }
    }
}
