let WSConnector = require('./websocket_connector')
const Constants = require('./constants')


const key   = process.env.EFX_KEY
const secret = process.env.EFX_SECRET


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


let connector = WSConnector.create(key, secret)
connector.connect(runClient, onMessage)


function onMessage(message) {
    if (message instanceof Array) {
        if (message[0] == 'I' && message[2] == 'S')
            onInstrumentsSnapshot(message)
        else if (message[0] == 'B')
            onGlobalBookUpdate(message)
    } else {
        onRequestResponse(message)
    }
}

async function runClient() {
    bind_to_global_book(connector, "BTC-USD")
    await sleep(5000);
    unbind_from_global_book_stream(connector, "BTC-USD")
    send_request_add_order(connector)
    connector.stop()
}


function bind_to_global_book(connector, feed_id) {
    // bind to global BTC_USD book stream
    connector.send({
        event: "bind",
        feed: "B",
        "feedId": feed_id
    })
}


function unbind_from_global_book_stream(connector, feed_id) {
    // unbind from global book stream
    connector.send({
        event: "unbind",
        feed: "B",
        "feedId": feed_id
    })
}

function send_request_add_order(connector) {
    /* add new order
       it just an example of taker's order
       if there is a level in BTC-USD that matches order's bid, a server returns deal(s)
       otherwise `77 - Add failed - FOK: not enough liquidity` error occurs
       which means there is no level in the book that matches this order
     */
    connector.send({
        event: "request",
        reqId: 12345,
        method: "add",
        content: {
            instrument: "BTC-USD",
            clientOrderId: 123456,
            price: 45000 * Constants.UNIT,
            size: 0.1 * Constants.UNIT,
            side: "bid",
            type: "limitFOK",
            cod: true // reasonable only for makers
        }
    })
}


function onInstrumentsSnapshot(data) {
    let currencies = data[3][0]
    let instruments = data[3][1]
    console.log("Currencies:", JSON.stringify(currencies))
    console.log("Instruments:", JSON.stringify(instruments))
}


function onRequestResponse(data) {
    console.log("Request response:", JSON.stringify(data))
}


function onGlobalBookUpdate(data) {
    if (data[2] == 'S') {
        console.log("Book snapshot received")
        console.log("Bids:", JSON.stringify(data[3][0]))
        console.log("Asks:", JSON.stringify(data[3][1]))
    } else {
        console.log("Book update received:", JSON.stringify(data[3]))
    }
}

