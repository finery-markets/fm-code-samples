const CryptoJS = require('crypto-js')
let WebSocketClient = require('websocket').w3cwebsocket

module.exports = {

    create: function(key, secret, demo=true) {
        return {
            key: key,
            secret: secret,
            host: demo ? 'wss://test.finerymarkets.com/ws' : 'wss://trade.finerymarkets.com/ws',

            connect: function (onConnect, onMessage) {
                let content = JSON.stringify({
                    "nonce": new Date().valueOf(),
                    "timestamp": new Date().valueOf()
                })

                let signature = CryptoJS.HmacSHA384(content, this.secret).toString(CryptoJS.enc.Base64)

                this.client = new WebSocketClient(this.host, null, null, {
                    'EFX-Key': this.key,
                    'EFX-Sign': signature,
                    'EFX-Content': content
                })

                this.client.onerror = function() {
                    console.log('Connection Error')
                }

                this.client.onopen = function() {
                    console.log('WebSocket Client Connected')
                    onConnect()
                }

                this.client.onclose = function() {
                    console.log('Client Closed')
                }

                this.client.onmessage = function(message) {
                    onMessage(JSON.parse(message.data))
                }
            },

            send: function(data) {
                const payload = JSON.stringify(data)
                this.client.send(payload)
            },

            stop: function() {
                this.client.close()
            }
        }
    }
}
