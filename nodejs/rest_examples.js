const RestConnector = require('./rest_connector')
const Constants = require('./constants')


const key = process.env.EFX_KEY
const secret = process.env.EFX_SECRET


function onError(response) {
    switch (response.status) {
        case 400: {
            let data = response.data
            let errorCode = data.error // data.error contains error code
            console.log("Error received:", errorCode)
            break
        }
        default: {
            // some unhandled error (connection error for example)
            console.log("Error received:", response)
            break
        }
    }
}

async function get_instruments(connector) {
    await connector.request('instruments', {})
        .then((data) => {
            console.log("Currencies:", JSON.stringify(data[0]))
            console.log("Instruments:", JSON.stringify(data[1]))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_positions(connector) {
    await connector.request('positions', {})
        .then((data) => {
            console.log("Next deal ID:", JSON.stringify(data[0]))
            console.log("Asset positions:", JSON.stringify(data[1]))
            console.log("Active orders:", JSON.stringify(data[2]))
            console.log("Active settlement orders:", JSON.stringify(data[3]))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_limits(connector) {
    return connector.request('limits', {})
        .then((data) => {
            data.forEach((limits) => {
                console.log("Global limit:")
                console.log("currency = ", limits[0])
                console.log("gross limit = ", limits[1])
                console.log("gross limit(used) = ", limits[2])
            })
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_counterparty_limits(connector) {
    return connector.request('climits', {})
        .then((data) => {
            data.forEach((limits) => {
                console.log("Counterparty limit: cp id = ", limits[0])
                console.log("currency = ", limits[1])
                console.log("gross limit = ", limits[2])
                console.log("mutual limit currency = ", limits[3])
                console.log("mutual gross limit = ", limits[4])
                console.log("gross limit(used) = ", limits[5])
                console.log("maintenance margin = ", limits[6])
                console.log("restricted trading = ", limits[7])
                console.log("initial margin = ", limits[8])
                console.log("equity = ", limits[9])
            })
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_settlement_requests(connector) {
    return connector.request('settlementRequests', {})
        .then((data) => {
            console.log("Incoming settlement requests", JSON.stringify(data[0]))
            console.log("Outgoing settlement requests", JSON.stringify(data[1]))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_settlement_transactions(connector) {
    return connector.request('settlementTransactions', {})
        .then((data) => {
            console.log("Incoming settlement transactions", JSON.stringify(data[0]))
            console.log("Outgoing settlement transactions", JSON.stringify(data[1]))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_book(connector) {
    return connector.request('book', {'instrument': 'BTC-USD', 'tradable': true})
        .then((data) => {
            console.log("Tradable book:")
            console.log("Bids", JSON.stringify(data[0]))
            console.log("Asks", JSON.stringify(data[1]))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function get_deal_history(connector) {
    const instrument = 'BTC-USD' // get deals for BTC-USD only
    let now = new Date()
    let start =  new Date()
    start.setDate(now.getDate() - 30)  // get deals for last 30 days
    let deals_to = now.valueOf()
    let deals_till = null

    let deals = []

    while(true) {
        let params = {
            instrument: instrument,
            from: start.valueOf(),
            limit: 250
        }
        if (deals_till)
            params['till'] = deals_till
        else
            params["to"] = deals_to

        let result = await connector.request('dealHistory', params)
            .catch(function (error) {
                onError(error.response)
            })

        if (result.length == 0)
            break

        deals_till = result[result.length - 1][11]
        deals = deals.concat(result)
    }

    console.log("Deals:", deals)
}

async function get_settlements_history(connector) {
    let now = new Date()
    let start =  new Date()
    start.setDate(now.getDate() - 30)  // get settlements for last 30 days
    let settlements_to = now.valueOf()
    let settlements_till = null

    let settlements = []

    while(true) {
        let params = {
            from: start.valueOf(),
            limit: 250
        }

        if (settlements_till)
            params['till'] = settlements_till
        else
            params["to"] = settlements_to

        let result = await connector.request('settlementHistory', params)
            .catch(function (error) {
                onError(error.response)
            })

        if (result.length == 0)
            break

        settlements_till = result[result.length - 1][10]
        settlements = settlements.concat(result)
    }

    console.log("Settlements:", settlements)
}

async function get_settlement_transactions_history(connector) {
    let now = new Date()
    let start =  new Date()
    start.setDate(now.getDate() - 30)  // get transactions for last 30 days
    let settlement_transactions_to = now.valueOf()
    let settlement_transactions_till = null

    let settlement_transactions = []

    while(true) {
        let params = {
            from: start.valueOf(),
            limit: 250
        }

        if (settlement_transactions_till)
            params['till'] = settlement_transactions_till
        else
            params["to"] = settlement_transactions_to

        let result = await connector.request('settlementTransactionHistory', params)
            .catch(function (error) {
                onError(error.response)
            })

        if (result.length == 0)
            break

        settlement_transactions_till = result[result.length - 1][10]
        settlement_transactions = settlement_transactions.concat(result)
    }

    console.log("Settlement transactions:", settlement_transactions)
}

async function add_order(connector) {
    /* add new order - Buy 2 BTC-USD @ 45000
       it just an example of taker's order
       if there is a level in BTC-USD that matches order's bid, a server returns deal(s)
       otherwise `77 - Add failed - FOK: not enough liquidity` error occurs
       which means there is no level in the book that matches this order
     */
    const params = {
        instrument: 'BTC-USD',
        clientOrderId: 42,
        price: 45500 * Constants.UINT,
        size: 1 * Constants.UINT,
        side: 'bid',
        type: 'limitFOK'
    }
    console.log("Add order")
    return connector.request('add', params)
        .then((data) => {
            console.log("Order execution result:", JSON.stringify(data))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function del_order(connector) {
    // it makes sense only for maker, since taker orders are executed or rejected instantly
    console.log("Del order")
    return connector.request('del', {orderId: 42})
        .then((data) => {
            console.log("Del order result:", JSON.stringify(data))
        }).catch(function (error) {
            onError(error.response)
        })
}

async function del_all_orders(connector) {
    // it makes sense only for maker, since taker orders are executed or rejected instantly
    console.log("Del all orders")
    return connector.request('delAll', {instrument: 'BTC-USD'})
        .then((data) => {
            console.log("Del all orders result:", JSON.stringify(data))
        }).catch(function (error) {
            onError(error.response)
        })
}

let connector = RestConnector.create(key, secret)
async function run_all() {
    await get_instruments(connector)
    await get_positions(connector)
    await get_limits(connector)
    await get_counterparty_limits(connector)
    await get_settlement_requests(connector)
    await get_settlement_transactions(connector)
    await get_book(connector)
    await get_deal_history(connector)
    await get_settlements_history(connector)
    await get_settlement_transactions_history(connector)
    await add_order(connector)
    await del_order(connector)
    await del_all_orders(connector)
}

run_all()
