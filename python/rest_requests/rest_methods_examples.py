import asyncio
import datetime
import os
from typing import Optional

import rest_connector


UNIT = 10**8


async def get_instruments(connector: rest_connector.Connector):
    result = await connector.request("instruments", {})
    print(f'Currencies = {result[0]}')
    print(f'Instruments = {result[1]}')


async def get_positions(connector: rest_connector.Connector):
    result = await connector.request("positions", {})
    print(f'Next deal id = {result[0]}')
    print(f'Asset positions = {result[1]}')
    print(f'Active orders = {result[2]}')
    print(f'Active settlement orders = {result[3]}')


async def get_limits(connector: rest_connector.Connector):
    result = await connector.request("limits", {})
    for entry in result:
        print(f'Global limit: currency = {entry[0]}, gross limit = {entry[1]}, gross limit(used) = {entry[2]}')


async def get_counterparty_limits(connector: rest_connector.Connector):
    result = await connector.request("climits", {})
    for entry in result:
        print(f'Counterparty limit: counterparty = {entry[0]}, currency = {entry[1]}, gross limit = {entry[2]}, '
              f'mutual limit currency = {entry[3]}, mutual gross limit = {entry[4]}, gross limit(used) = {entry[5]}, '
              f'maintenance margin = {entry[6]}, restricted trading = {entry[7]}, initial margin = {entry[8]}, '
              f'equity = {entry[9]}')


async def get_settlement_requests(connector: rest_connector.Connector):
    result = await connector.request("settlementRequests", {})
    print(f"Incoming settlement requests: {result[0]}")
    print(f"Outgoing settlement requests: {result[1]}")


async def get_settlement_transactions(connector: rest_connector.Connector):
    result = await connector.request("settlementTransactions", {})
    print(f"Incoming settlement transactions: {result[0]}")
    print(f"Outgoing settlement transactions: {result[1]}")


async def get_book(connector: rest_connector.Connector):
    result = await connector.request("book", {
        "instrument": 'BTC-USD',
        "tradable": True,
    })
    print(f"Bids: {result[0]}")
    print(f"Asks: {result[1]}")


async def get_deal_history(connector: rest_connector.Connector):
    instrument = 'BTC-USD'  # get deals for BTC-USD only
    now = datetime.datetime.utcnow()
    start = now - datetime.timedelta(days=30)  # get deals for last 30 days
    deals_to = int(1e3*now.timestamp())
    deals_from = int(1e3*start.timestamp())
    deals_till: Optional[int] = None
    deals = []
    while True:
        params = {
            "from": deals_from,
            "instrument": instrument,
            "limit": 250,
        }
        if deals_till is not None:
            params["till"] = deals_till
        else:
            params["to"] = deals_to
        
        result = await connector.request("dealHistory", params)
        if len(result) == 0:
            break
        deals_till = min(d[11] for d in result)
        deals += result

    print(f'Deals history - {len(deals)} deals')
    for deal in deals:
        print(deal)


async def get_settlement_history(connector: rest_connector.Connector):
    now = datetime.datetime.utcnow()
    start = now - datetime.timedelta(days=30)  # get deals for last 30 days
    settlements_to = int(1e3*now.timestamp())
    settlements_from = int(1e3*start.timestamp())
    settlements_till: Optional[int] = None
    settlements = []
    while True:
        params = {
            "from": settlements_from,
            "limit": 250,
        }
        if settlements_till:
            params["till"] = settlements_till
        else:
            params["to"] = settlements_to
        
        result = await connector.request("settlementHistory", params)
        if len(result) == 0:
            break
        settlements_till = min(s[10] for s in result)
        settlements += result

    print(f'Settlements history - {len(settlements)}')
    for settlement in settlements:
        print(settlement)


async def get_settlement_transaction_history(connector: rest_connector.Connector):
    now = datetime.datetime.utcnow()
    start = now - datetime.timedelta(days=30)  # get deals for last 30 days
    settlement_transactions_to = int(1e3*now.timestamp())
    settlement_transactions_from = int(1e3*start.timestamp())
    settlement_transactions_till: Optional[int] = None
    settlement_transactions = []
    while True:
        params = {
            "from": settlement_transactions_from,
            "limit": 250,
        }
        if settlement_transactions_till is not None:
            params["till"] = settlement_transactions_till
        else:
            params["to"] = settlement_transactions_to
        
        result = await connector.request("settlementTransactionHistory", params)
        if len(result) == 0:
            break
        settlement_transactions_till = min(s[10] for s in result)
        settlement_transactions += result

    print(f'Transactions history: - {len(settlement_transactions)}')
    for transaction in settlement_transactions:
        print(transaction)


# Buy 2 BTC-USD @ 45000
async def add_order(connector: rest_connector.Connector):
    # add new order
    # it just an example of taker's order
    # if there is a level in BTC-USD that matches order's bid, a server returns deal(s)
    # otherwise `77 - Add failed - FOK: not enough liquidity` error occurs
    # which means there is no level in the book that matches this order
    instrument = 'BTC-USD'
    client_order_id = 42
    price = 45500 * UNIT
    size = 1 * UNIT
    side = 'bid'
    order_type = 'limitFOK'
    cancel_on_disconnect = True

    result = await connector.request('add', {
            "instrument": instrument,
            "clientOrderId": client_order_id,
            "price": price,
            "size": size,
            "side": side,
            "type": order_type,
            "cod": cancel_on_disconnect,
    })
    print(f'Add order: {result}')


# it makes sense only for maker
async def del_order(connector: rest_connector.Connector):
    # del by client order id
    result = await connector.request('del', {
            "clientOrderId": 42,
    })
    print(f'Del order by client order id: {result}')

    # del by exchange order id (from add response)
    result = await connector.request('del', {
            "orderId": 42,
    })
    print(f'Del order by finery order id: {result}')


async def del_all_orders(connector: rest_connector.Connector):
    # delete all BTC-USD orders
    result = await connector.request('delAll', {
            "instrument": 'BTC-USD',
    })
    print(f'Del all orders for instrument: {result}')

    # delete all orders for all instruments
    result = await connector.request('delAll', {})
    print(f'Del all orders: {result}')


async def run_all():
    key = os.getenv("EFX_KEY")
    secret = os.getenv("EFX_SECRET")
    connector = rest_connector.Connector(key, secret, True)
    await get_instruments(connector)
    await get_positions(connector)
    await get_limits(connector)
    await get_counterparty_limits(connector)
    await get_settlement_requests(connector)
    await get_settlement_transactions(connector)
    await get_book(connector)
    await get_deal_history(connector)
    await get_settlement_history(connector)
    await get_settlement_transaction_history(connector)
    await add_order(connector)
    await del_order(connector)
    await del_all_orders(connector)


if __name__ == "__main__":
    asyncio.run(run_all())
