# Python 3.8 required

import aiohttp
import base64
import json
import hmac
import hashlib
import time
from typing import Dict, Any


class Connector:
    def __init__(self, key: str, secret: str, is_demo: bool) -> None:
        self._key = key
        self._secret = secret
        self._host = 'https://test.finerymarkets.com' if is_demo else 'https://trade.finerymarkets.com'

    async def request(self, method: str, content: Dict[str, Any]) -> Any:
        content["nonce"] = int(round(time.time_ns() / 1000000))
        content["timestamp"] = int(round(time.time_ns() / 1000000))
        content_str: str = json.dumps(content)
        signature_hmac: hmac = hmac.new(
            self._secret.encode('ascii'),
            (method + content_str).encode('ascii'),
            hashlib.sha384)
        signature = base64.b64encode(signature_hmac.digest()).decode('ascii')
        headers: Dict[str, str] = {
            'EFX-Key': self._key,
            'EFX-Sign': signature,
            'Content-Type': 'application/json',
        }

        async with aiohttp.ClientSession(headers=headers) as session:
            url = f'{self._host}/api/{method}'
            async with session.post(url=url, data=content_str) as response:
                data = await response.json()
                return data
