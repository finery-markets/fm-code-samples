# Python 3.8 required

import base64
import hashlib
import hmac
import json
import os
import ssl
import time

import logging
from dataclasses import dataclass
from operator import neg
from typing import List, Mapping

import asyncio

import aiohttp
import sortedcontainers  # pip install sortedcontainers


UNIT = 10**8
logging.basicConfig(format='%(asctime)s.%(msecs)06f %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.INFO)  # To check Web-sockets message, please use DEBUG level


@dataclass
class Book:
    iid: str
    bid_snapshot: Mapping[float, float]
    ask_snapshot: Mapping[float, float]


RESTART_TIMEOUT_IN_SEC = 60
PRINT_BOOK_TIMEOUT = 30


def normalize_efx(s):
    return float(s) / UNIT


class BookConnectorEfx(object):

    def __init__(self, symbols: List[str], is_demo: bool = True):
        self.symbols = symbols
        self.ws_url = 'wss://test.finerymarkets.com/ws' if is_demo else 'wss://trade.finerymarkets.com/ws'
        self.key = os.getenv("KEY")
        self.secret = os.getenv("SECRET")
        self._ws = None
        self.books = {}

    def _prepare_auth_headers(self):
        content = {
            'nonce': time.time_ns(),
            'timestamp': int(round(time.time_ns() / 1000000)),
        }
        content_str = json.dumps(content)
        signature_hmac: hmac = hmac.new(
            self.secret.encode('ascii'),
            content_str.encode('ascii'),
            hashlib.sha384)
        signature = base64.b64encode(signature_hmac.digest()).decode('ascii')
        headers = {
            'EFX-Content': content_str,
            'EFX-Key': self.key,
            'EFX-Sign': signature
        }
        return headers

    async def fetcher_session(self):
        async with aiohttp.ClientSession() as session:  # type: aiohttp.ClientSession
            headers = self._prepare_auth_headers()
            async with session.ws_connect(self.ws_url, ssl=ssl.SSLContext(), headers=headers) as ws:
                await self.subscribe_handler(ws)
                async for msg in ws:  # type: aiohttp.WSMessage
                    if msg.type == aiohttp.WSMsgType.TEXT:
                        await self.message_handler(msg.data)
                    elif msg.type == aiohttp.WSMsgType.ERROR:
                        break

    async def subscribe_handler(self, ws):
        self._ws = ws
        while True:
            try:
                data = await self._ws.receive_str()
            except Exception as e:
                logging.error(f'Error during handling init-data: {str(e)}')
                return
            logging.info(data)
            message = json.loads(data)
            if isinstance(message, list) and message[0] == 'X':
                error = message[3]
                if error == 0:
                    await self.subscribe_global_order_books()
                    return
                else:
                    logging.error(f'connection error: {error}')
                    return

    async def subscribe_global_order_book(self, symbol):
        req_str = json.dumps({
            'event': 'bind', 'feed': 'B', 'feedId': symbol
        })
        await self._ws.send_str(req_str)
        bids = sortedcontainers.SortedDict(neg)
        asks = sortedcontainers.SortedDict()
        self.books[symbol] = Book(
            iid=symbol,
            bid_snapshot=bids,
            ask_snapshot=asks,
        )

    async def unsubscribe_global_order_book(self, feed_id):
        req_str = json.dumps({
            'event': 'unbind', 'feed': 'B', 'feedId': feed_id
        })
        await self._ws.send_str(req_str)
        del self.books[feed_id]

    async def subscribe_global_order_books(self):
        for symbol in self.symbols:
            feed_symbol = symbol.replace('/', '-')
            await self.subscribe_global_order_book(feed_symbol)

    def edit_levels(self, levels: sortedcontainers.SortedDict, operation: str, price: float, size: float):
        if operation in ['+', 'M']:
            levels[price] = size
        elif operation == '-':
            del levels[price]
        elif operation == '~':
            if price == 0:
                logging.warning(f'Price is {price}, clear all levels {levels}.')
                levels.clear()
                return
            for level_price in levels:
                if level_price != price:
                    del levels[level_price]
                    return
                else:
                    levels[price] = size
                    return
            logging.warning(f'Level with price {price} not found: {levels}!')

    async def process_global_order_books(self, message):
        feed_id = message[1]
        operation_type = message[2]
        if 'S' == operation_type:  # global book snapshot
            bids, asks = message[3]
            # e.g. ["B",3895299724,"S", [bids, asks]]
            for price, size in bids:
                self.books[feed_id].bid_snapshot[normalize_efx(price)] = normalize_efx(size)
            for price, size in asks:
                self.books[feed_id].ask_snapshot[normalize_efx(price)] = normalize_efx(size)
        elif 'M' == operation_type:  # modify
            bids, asks = message[3]
            for operation, price, size in bids:
                self.edit_levels(self.books[feed_id].bid_snapshot, operation, normalize_efx(price), normalize_efx(size))
            for operation, price, size in asks:
                self.edit_levels(self.books[feed_id].ask_snapshot, operation, normalize_efx(price), normalize_efx(size))
        else:
            logging.warning(f'process_global_order_books: {message}')

    async def message_handler(self, data):
        if not data:
            logging.error('no data')
            return
        message = json.loads(data)
        if isinstance(message, list):
            if 'B' == message[0]:
                # Global order book
                await self.process_global_order_books(message)
            else:
                logging.debug(f'unexpected message: {message}')

    async def print_books(self):
        while True:
            await asyncio.sleep(PRINT_BOOK_TIMEOUT)
            for book in self.books.values():
                bids = {k: v for k, v in book.bid_snapshot.items()}
                asks = {k: v for k, v in book.ask_snapshot.items()}
                logging.info(f'{book.iid}:\nBIDS: {bids}\nASKS: {asks}')

    async def start(self):
        while True:
            try:
                tasks = [
                    self.fetcher_session(),
                    self.print_books(),
                ]

                done, pending = await asyncio.wait([asyncio.create_task(task) for task in tasks],
                                                   return_when=asyncio.FIRST_EXCEPTION)

                for task in done:
                    if task.exception() is not None:
                        logging.error('Task exited with exception:')
                        task.print_stack()
                        logging.warning('Rescheduling the task.')
                for task in pending:
                    task.cancel()
            except asyncio.exceptions.CancelledError as cancelledError:
                logging.error('asyncio.exceptions.CancelledError occurred.')
                logging.error(cancelledError, stack_info=True)
            except Exception as e:
                logging.exception(e, stack_info=True)
            finally:
                await asyncio.sleep(RESTART_TIMEOUT_IN_SEC)


async def main():
    # Parse 'BTC/USD', 'BTC/EUR' books, maintain them and print out their values every 30 seconds
    await BookConnectorEfx(['BTC/USD', 'BTC/EUR']).start()


if __name__ == '__main__':
    asyncio.run(main())
