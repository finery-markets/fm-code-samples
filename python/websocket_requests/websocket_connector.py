import asyncio
import base64
import json
import hmac
import hashlib
import time
import websockets
from abc import abstractmethod
from typing import Any


class MessageProcessor:
    @abstractmethod
    def process_message(self, msg: Any) -> None:
        pass


class WebsocketsConnector:
    def __init__(self, key: str, secret: str, is_demo: bool, message_processor: MessageProcessor):
        self._key = key
        self._secret = secret
        self._host = 'wss://test.finerymarkets.com/ws' if is_demo else 'wss://trade.finerymarkets.com/ws'
        self._message_processor = message_processor
        self._connected_event = asyncio.Event()

    async def connect(self):
        # Generate signature for authentication
        content = {
            'nonce': int(round(time.time_ns() / 1000000)),
            'timestamp': int(round(time.time_ns() / 1000000)),
        }
        content_str = json.dumps(content)
        signature_hmac: hmac = hmac.new(
            self._secret.encode('ascii'),
            content_str.encode('ascii'),
            hashlib.sha384)
        signature = base64.b64encode(signature_hmac.digest()).decode('ascii')

        # Add authentication headers to ws connection request
        extra_headers = {
            'EFX-Key': self._key,
            'EFX-Sign': signature,
            'EFX-Content': content_str
        }

        # Connect websocket
        self._client: websockets.WebSocketCommonProtocol = await websockets.connect(
            self._host,
            compression=None,
            extra_headers=extra_headers)

        asyncio.create_task(self._listen())
        await self._connected_event.wait()

    async def send(self, message: Any):
        await self._client.send(json.dumps(message))

    async def _listen(self):
        while True:
            data = await self._client.recv()
            message = json.loads(data)
            print(message)
            if isinstance(message, list):
                if message[0] == 'X':
                    error = message[3]
                    if error == 0:
                        self._connected_event.set()
                        continue
                    else:
                        print(f'connection error: {error}')
                        return
            self._message_processor.process_message(message)
