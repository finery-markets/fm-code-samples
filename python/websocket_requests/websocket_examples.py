import asyncio
import logging
import os
from typing import Any

from websocket_connector import MessageProcessor, WebsocketsConnector


logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.INFO)  # To check Web-sockets message, please use DEBUG level
ws_messages = []
UNIT = 10**8


class MessagePrinter(MessageProcessor):
    def process_message(self, msg: Any):
        print(msg)
        ws_messages.append(msg)


async def bind_to_global_book(connector: WebsocketsConnector, feed_id: str):
    # bind to books stream
    await connector.send({
        "event": "bind",
        "feed": "B",
        "feedId": feed_id,
    })


async def bind_to_position_stream(connector: WebsocketsConnector):
    # bind to positions stream
    await connector.send({
        "event": "bind",
        "feed": "P",
    })


async def unbind_from_position_stream(connector: WebsocketsConnector):
    # unbind from positions stream
    await connector.send({
        "event": "unbind",
        "feed": "P",
    })


async def unbind_from_global_book_stream(connector: WebsocketsConnector, feed_id: str):
    # unbind from books stream
    await connector.send({
        "event": "unbind",
        "feed": "B",
        "feedId": feed_id,
    })


async def send_request_add_order(connector: WebsocketsConnector):
    # add new order
    # it just an example of taker's order
    # if there is a level in BTC-USD that matches order's bid, a server returns deal(s)
    # otherwise `77 - Add failed - FOK: not enough liquidity` error occurs
    # which means there is no level in the book that matches this order
    await connector.send({
        "event": "request",
        "reqId": 12345,
        "method": "add",
        "content": {
            "instrument": "BTC-USD",
            "clientOrderId": 123456,
            "price": 45000*UNIT,
            "size": 0.1*UNIT,
            "side": "bid",
            "type": "limitFOK",
            "cod": True,
        } 
    })


async def send_request_del_order(connector: WebsocketsConnector):
    # cancel a deal (it makes sense only for maker)
    await connector.send({
        "event": "request",
        "reqId": 1234567,
        "method": "del",
        "content": {
            "clientOrderId": 123456,
        } 
    })


async def run_all():
    key = os.getenv("EFX_KEY")
    secret = os.getenv("EFX_SECRET")
    connector = WebsocketsConnector(key, secret, True, MessagePrinter())
    await connector.connect()
    await asyncio.sleep(1)
    # Bind to positions stream and print positions
    await bind_to_position_stream(connector)
    # Subscribe to global order book feed
    await bind_to_global_book(connector, "BTC-USD")
    # Wait for 5 seconds and print out global book updates for BTC-USD instrument
    await asyncio.sleep(5)
    await unbind_from_position_stream(connector)
    await unbind_from_global_book_stream(connector, "BTC-USD")
    await asyncio.sleep(1)
    await send_request_add_order(connector)
    # it makes sense only for maker
    await send_request_del_order(connector)
    await asyncio.sleep(1.0)


if __name__ == "__main__":
    asyncio.run(run_all())
