package finery.samples.fix;

import quickfix.ConfigError;
import quickfix.Initiator;
import quickfix.Message;
import quickfix.Session;
import quickfix.SessionNotFound;

import java.util.logging.Logger;

public class FixInitiator {

    private static final Logger LOGGER = Logger.getLogger(FixInitiator.class.getName());

    private final Initiator initiator;

    public FixInitiator(Initiator initiator) {
        this.initiator = initiator;
    }

    public void start() {
        try {
            initiator.start();
        } catch (ConfigError ex) {
            LOGGER.severe("Initiator can not be started " + ex.getMessage());
        }
    }


    public boolean send(Message msg) {
        var sessions = initiator.getSessions();
        for (var sessionId : sessions) {
            try {
                //we expect that only single session is present
                return Session.sendToTarget(msg, sessionId);
            } catch (SessionNotFound ex) {
                LOGGER.severe("Unable to send to SessionId: " + sessionId + ". " + ex.getMessage());
            }
        }
        return false;
    }

    public void stop() {
        initiator.stop();
    }
}
