package finery.samples.fix;

public class FixSamplesDemo {

    public static void run() {
        var connector = new FixConnector();
        connector.start();
    }
}
