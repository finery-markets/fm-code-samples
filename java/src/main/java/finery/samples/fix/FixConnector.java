package finery.samples.fix;

import finery.samples.utils.SigningUtils;
import quickfix.ApplicationFunctionalAdapter;
import quickfix.ConfigError;
import quickfix.DefaultMessageFactory;
import quickfix.FieldNotFound;
import quickfix.MemoryStoreFactory;
import quickfix.ScreenLogFactory;
import quickfix.SessionSettings;
import quickfix.StringField;
import quickfix.ThreadedSocketInitiator;
import quickfix.field.ClOrdID;
import quickfix.field.MDEntryType;
import quickfix.field.MDReqID;
import quickfix.field.MDUpdateType;
import quickfix.field.MsgType;
import quickfix.field.OrdType;
import quickfix.field.OrderID;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Password;
import quickfix.field.Price;
import quickfix.field.RawData;
import quickfix.field.RawDataLength;
import quickfix.field.SenderCompID;
import quickfix.field.Side;
import quickfix.field.SubscriptionRequestType;
import quickfix.field.Symbol;
import quickfix.field.Text;
import quickfix.field.TimeInForce;
import quickfix.field.TransactTime;
import quickfix.field.Username;
import quickfix.fix44.ExecutionReport;
import quickfix.fix44.Logon;
import quickfix.fix44.MarketDataIncrementalRefresh;
import quickfix.fix44.MarketDataRequest;
import quickfix.fix44.MarketDataRequestReject;
import quickfix.fix44.MarketDataSnapshotFullRefresh;
import quickfix.fix44.NewOrderSingle;
import quickfix.fix44.OrderCancelReject;
import quickfix.fix44.OrderCancelReplaceRequest;
import quickfix.fix44.OrderCancelRequest;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;


public class FixConnector {
    private static final Logger LOGGER = Logger.getLogger(FixConnector.class.getName());

    private final FixInitiator fixInitiator;
    private final AtomicBoolean logonSuccessful = new AtomicBoolean(false);

    public FixConnector() {
        this.fixInitiator = init();
    }

    private static MarketDataRequest buildMarketDataSubscribeRequest() {
        var marketDataRequest = new MarketDataRequest();
        marketDataRequest.set(new MDReqID(UUID.randomUUID().toString()));
        marketDataRequest.set(new SubscriptionRequestType(SubscriptionRequestType.SNAPSHOT_UPDATES));
        // Required if SubscriptionRequestType<263> is 1.
        marketDataRequest.set(new MDUpdateType(MDUpdateType.INCREMENTAL_REFRESH));
        //always 2 for bid and offer
        //https://www.quickfixj.org/usermanual/2.3.0/usage/repeating_groups.html
        var noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();
        noMDEntryTypes.set(new MDEntryType(MDEntryType.BID));
        marketDataRequest.addGroup(noMDEntryTypes);
        noMDEntryTypes.set(new MDEntryType(MDEntryType.OFFER));
        marketDataRequest.addGroup(noMDEntryTypes);
        var noRelatedSym = new MarketDataRequest.NoRelatedSym();
        noRelatedSym.set(new Symbol("BTC-USD"));
        marketDataRequest.addGroup(noRelatedSym);
        return marketDataRequest;
    }

    private static MarketDataRequest buildMarketDataUnsubscribeRequest() {
        var marketDataRequest = new MarketDataRequest();
        marketDataRequest.set(new MDReqID(UUID.randomUUID().toString()));
        marketDataRequest.set(new SubscriptionRequestType(SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_UPDATE_REQUEST));
        //always 2 for bid and offer
        //https://www.quickfixj.org/usermanual/2.3.0/usage/repeating_groups.html
        var noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();
        noMDEntryTypes.set(new MDEntryType(MDEntryType.BID));
        marketDataRequest.addGroup(noMDEntryTypes);
        noMDEntryTypes.set(new MDEntryType(MDEntryType.OFFER));
        marketDataRequest.addGroup(noMDEntryTypes);
        var noRelatedSym = new MarketDataRequest.NoRelatedSym();
        noRelatedSym.set(new Symbol("BTC-USD"));
        marketDataRequest.addGroup(noRelatedSym);
        return marketDataRequest;
    }

    private static NewOrderSingle buildNewOrderSingle() {
        var newOrderSingle = new NewOrderSingle();
        newOrderSingle.set(new ClOrdID(String.valueOf(System.currentTimeMillis())));
        newOrderSingle.set(new Symbol("BTC-USD"));
        //Price class uses double as field type in constructor for compatibility with quickfix for c++
        //So this expression is used to avoid floating point numbers precision problem
        var price = new StringField(Price.FIELD, "3603936598382");
        newOrderSingle.setField(price);
        // if the order is by volume
        //var cashOrderQty = new StringField(CashOrderQty.FIELD, "100000000");
        //newOrderSingle.setField(cashOrderQty);
        // if the order is by size
        var orderQty = new StringField(OrderQty.FIELD, "10");
        newOrderSingle.setField(orderQty);
        newOrderSingle.set(new Side(Side.SELL));
        newOrderSingle.set(new OrdType(OrdType.LIMIT));
        newOrderSingle.set(new TimeInForce(TimeInForce.GOOD_TILL_CANCEL));
        newOrderSingle.set(new TransactTime());
        return newOrderSingle;
    }

    private static OrderCancelRequest buildOrderCancelRequest() {
        var orderCancelRequest = new OrderCancelRequest();
        //Either OrderID<37> or OrigClOrdID<41> should be present.
        //orderCancelRequest.set(new OrderID("123"));
        orderCancelRequest.set(new ClOrdID(String.valueOf(System.currentTimeMillis())));
        orderCancelRequest.set(new OrigClOrdID("123456"));
        orderCancelRequest.set(new Side(Side.BUY));
        orderCancelRequest.set(new TransactTime());
        return orderCancelRequest;
    }

    private static OrderCancelReplaceRequest buildOrderCancelReplaceRequest() {
        var orderCancelReplaceRequest = new OrderCancelReplaceRequest();
        orderCancelReplaceRequest.set(new OrderID("123455"));
        orderCancelReplaceRequest.set(new ClOrdID(String.valueOf(System.currentTimeMillis())));
        var price = new StringField(Price.FIELD, "3603936598382");
        orderCancelReplaceRequest.setField(price);
        var orderQty = new StringField(OrderQty.FIELD, "2300");
        orderCancelReplaceRequest.setField(orderQty);
        orderCancelReplaceRequest.set(new OrdType(OrdType.LIMIT));
        orderCancelReplaceRequest.set(new Side(Side.BUY));
        orderCancelReplaceRequest.set(new TransactTime());
        return orderCancelReplaceRequest;
    }

    private static void addCustomDataToLogon(Logon logon) throws FieldNotFound {
        var secret = System.getenv("SECRET");
        var nonce = System.currentTimeMillis();
        var rawData = "{\"nonce\":" + nonce + ",\"timestamp\":" + nonce + "}";
        logon.set(new RawData(rawData));
        logon.set(new RawDataLength(rawData.length()));
        var senderCompId = logon.getHeader().getString(SenderCompID.FIELD);
        logon.set(new Username(senderCompId));
        var signature = SigningUtils.getSignature(rawData, secret);
        logon.set(new Password(signature));
    }

    private FixInitiator init() {
        try {
            var sessionSettings = new SessionSettings("fix.cfg");

            //this adapter allows you to implement FIX event listeners in a functional way,
            //but also you may consider implementing quickfix.Application class and overriding its methods instead of this
            var application = new ApplicationFunctionalAdapter();

            configureEventHandlers(application);

            //see quickfix.MessageStoreFactory implementations for different types of message storing
            var messageStoreFactory = new MemoryStoreFactory();
            //see quickfix.LogFactory implementations for different types of logging
            var logFactory = new ScreenLogFactory(true, true, true);
            var messageFactory = new DefaultMessageFactory();
            var initiator = new ThreadedSocketInitiator(
                    application,
                    messageStoreFactory,
                    sessionSettings,
                    logFactory,
                    messageFactory);
            return new FixInitiator(initiator);
        } catch (ConfigError ex) {
            LOGGER.severe("Error during connector configuration" + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    /**
     * This method is used to add event listeners for pre-processing requests and handling responses
     */
    private void configureEventHandlers(ApplicationFunctionalAdapter application) {
        application.addToAdminListener((message, sessionID) -> {
            try {
                var header = message.getHeader();
                var msgType = header.getString(MsgType.FIELD);
                if (MsgType.LOGON.equals(msgType)) {
                    addCustomDataToLogon((Logon) message);
                }
            } catch (FieldNotFound e) {
                throw new RuntimeException(e);
            }
        });
        application.addOnLogonListener(sessionId -> logonSuccessful.set(true));
        application.addOnLogoutListener(sessionId -> LOGGER.info("Successful logout for session with id " + sessionId));
        application.addFromAppListener(MarketDataSnapshotFullRefresh.class, (response, sessionID) -> LOGGER.info("Received MarketDataSnapshotFullRefresh response " + response.toString()));
        application.addFromAppListener(MarketDataIncrementalRefresh.class, (response, sessionID) -> LOGGER.info("Received MarketDataIncrementalRefresh response " + response.toString()));
        application.addFromAppListener(MarketDataRequestReject.class, (response, sessionID) -> LOGGER.info("Received MarketDataRequestReject response. Reject reason is " + response.get(new Text()).getValue()));
        application.addFromAppListener(ExecutionReport.class, (response, sessionID) -> LOGGER.info("Received ExecutionReport response " + response));
        application.addFromAppListener(OrderCancelReject.class, (response, sessionID) -> LOGGER.info("Received OrderCancelReject response " + response.toString()));
    }

    public void start() {
        try {
            fixInitiator.start();
            var start = System.currentTimeMillis();
            while (!logonSuccessful.get()) {
                if ((System.currentTimeMillis() - start) / 1000L > 30) {
                    throw new RuntimeException("Logon was not successful");
                }
                LOGGER.info("Waiting for logon");
                TimeUnit.SECONDS.sleep(1L);
            }
            var marketDataSubscribeRequest = buildMarketDataSubscribeRequest();
            fixInitiator.send(marketDataSubscribeRequest);
            TimeUnit.SECONDS.sleep(5L);
            var marketDataUnsubscribeRequest = buildMarketDataUnsubscribeRequest();
            fixInitiator.send(marketDataUnsubscribeRequest);
            TimeUnit.SECONDS.sleep(1L);
            var newOrderSingle = buildNewOrderSingle();
            fixInitiator.send(newOrderSingle);
            TimeUnit.SECONDS.sleep(1L);
            var orderCancelReplaceRequest = buildOrderCancelReplaceRequest();
            fixInitiator.send(orderCancelReplaceRequest);
            TimeUnit.SECONDS.sleep(1L);
            var orderCancelRequest = buildOrderCancelRequest();
            fixInitiator.send(orderCancelRequest);
            TimeUnit.SECONDS.sleep(1L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            fixInitiator.stop();
        }
    }
}
