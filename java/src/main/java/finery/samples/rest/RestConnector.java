package finery.samples.rest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import finery.samples.utils.SigningUtils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RestConnector {
    final private String key;
    final private String secret;
    final private String host;
    final private HttpClient httpClient;

    public RestConnector(String key, String secret, Boolean isDemo) {
        this.key = key;
        this.secret = secret;
        this.host = isDemo ? "https://test.finerymarkets.com" : "https://trade.finerymarkets.com";
        httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    }

    public String request(String method, Map<String, Object> content) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InterruptedException {
        content = new HashMap<>(content);
        long timeMilli = new Date().getTime();
        content.put("nonce", timeMilli);
        content.put("timestamp", timeMilli);

        var gson = new Gson();
        var typeObject = new TypeToken<HashMap<String, Object>>() {}.getType();
        var contentStr = gson.toJson(content, typeObject);
        var signature = SigningUtils.getSignature(method + contentStr, secret);

        HttpRequest request = HttpRequest.newBuilder(URI.create(host + "/api/" + method))
                .header("EFX-Key", key)
                .header("EFX-Sign", signature)
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(contentStr))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }
}
