package finery.samples.rest;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RestSamples {
    private final static int ENTITIES_LIMIT = 250;
    private final static Logger LOGGER = Logger.getLogger(RestSamples.class.getName());
    private RestConnector connector;

    public RestSamples(RestConnector connector) {
        this.connector = connector;
    }

    public void showPositions() throws Exception {
        var positions = new JSONArray(this.connector.request("positions", Map.of()));
        LOGGER.info("Positions::Next id: " + positions.get(0));
        LOGGER.info("Positions::Asset positions: " + positions.get(1));
        LOGGER.info("Positions::Active orders: " + positions.get(2));
        LOGGER.info("Positions::Active settlement orders: " + positions.get(3));
    }

    public void showInstruments() throws Exception {
        var instruments = new JSONArray(this.connector.request("instruments", Map.of()));
        LOGGER.info("Instruments::Currencies: " + instruments.get(0));
        LOGGER.info("Instruments::Instruments: " + instruments.get(1));
        LOGGER.info("Instruments::Networks: " + instruments.get(2));
    }

    public void showLimits() throws Exception {
        var limits = new JSONArray(this.connector.request("limits", Map.of()));
        LOGGER.info("Limit::Currency: " + ((JSONArray) limits.get(0)).get(0));
        LOGGER.info("Limit::Gross limit: " + ((JSONArray) limits.get(0)).get(1));
        LOGGER.info("Limit::Gross limit utilisation: " + ((JSONArray) limits.get(0)).get(2));
    }

    public void showCpLimits() throws Exception {
        var limits = new JSONArray(this.connector.request("climits", Map.of()));
        for (int i = 0; i < limits.length(); i++) {
            LOGGER.info("CP Limit:: CP=" + ((JSONArray) limits.get(i)).get(0) + ": " + limits.get(i));
        }
    }

    public void showSettlementRequests() throws Exception {
        var settlementRequests = new JSONArray(this.connector.request("settlementRequests", Map.of()));
        LOGGER.info("Settlement Requests::Incoming: " + settlementRequests.get(0));
        LOGGER.info("Settlement Requests::Outgoing: " + settlementRequests.get(1));
    }

    public void showSettlementTransactions() throws Exception {
        var settlementTransactions = new JSONArray(this.connector.request("settlementTransactions", Map.of()));
        LOGGER.info("Settlement Transactions::Incoming: " + settlementTransactions.get(0));
        LOGGER.info("Settlement Transactions::Outgoing: " + settlementTransactions.get(1));
    }

    public void showBook(String instrument, Boolean isTradable) throws Exception {
        var book = new JSONArray(this.connector.request("book",
                Map.of("instrument", instrument, "isTradable", isTradable)));
        LOGGER.info(String.format("Book %s Tradable=%b::bids: %s", instrument, isTradable, book.get(0)));
        LOGGER.info(String.format("Book %s Tradable=%b::asks: %s", instrument, isTradable, book.get(1)));
    }

    public void showDealHistory() throws NoSuchAlgorithmException, InvalidKeyException, JSONException, IOException, InterruptedException {
        var instrument = "BTC-USD";
        var now = Instant.now();
        //  get deals for the last 24 hours
        var start = now.minus(24, ChronoUnit.HOURS);
        var dateTo = now.toEpochMilli();
        var dateFrom = start.toEpochMilli();
        long tillDealId = 0, date;
        var deals = new JSONArray();
        Map<String, Object> params;

        while (true) {
            params = new HashMap<>() {{
                put("from", dateFrom);
                put("instrument", instrument);
                put("limit", ENTITIES_LIMIT);
            }};
            if (tillDealId != 0) {
                params.put("till", tillDealId);
            } else {
                params.put("to", dateTo);
            }

            var result = new JSONArray(this.connector.request("dealHistory", params));
            if (result.length() == 0) {
                break;
            }
            for (int index = 0; index < result.length(); index++) {
                date = (long) ((JSONArray) result.get(index)).get(10);
                if (date < dateFrom) {
                    break;
                }
                deals.put(result.getJSONArray(index));
                tillDealId = (int) ((JSONArray) result.get(index)).get(11);
            }
        }

        LOGGER.info(String.format("Deals history for %s within last 24 hours:\n%s", instrument, deals));
    }

    public void showSettlementHistory() throws NoSuchAlgorithmException, InvalidKeyException, JSONException, IOException, InterruptedException {
        Instant now = Instant.now();
        //  get settlements for the last 24 hours
        Instant start = now.minus(24, ChronoUnit.HOURS);
        var dateTo = now.toEpochMilli();
        var dateFrom = start.toEpochMilli();
        long tillTransactionId = 0, date;
        var settlements = new JSONArray();
        Map<String, Object> params;

        while (true) {
            params = new HashMap<>() {{
                put("from", dateFrom);
                put("limit", ENTITIES_LIMIT);
            }};
            if (tillTransactionId != 0) {
                params.put("till", tillTransactionId);
            } else {
                params.put("to", dateTo);
            }

            var result = new JSONArray(this.connector.request("settlementHistory", params));
            if (result.length() == 0) {
                break;
            }
            for (int index = 0; index < result.length(); index++) {
                date = (long) ((JSONArray) result.get(index)).get(9);
                if (date < dateFrom) {
                    break;
                }
                settlements.put(result.getJSONArray(index));
                tillTransactionId = (int) ((JSONArray) result.get(index)).get(10);
            }
        }

        LOGGER.info(String.format("Settlement history within last 24 hours:\n%s", settlements));
    }

    public void showSettlementTransactionHistory() throws NoSuchAlgorithmException, InvalidKeyException, JSONException, IOException, InterruptedException {
        var now = Instant.now();
        //  get settlement transactions for the last 24 hours
        var start = now.minus(24, ChronoUnit.HOURS);
        var dateTo = now.toEpochMilli();
        var dateFrom = start.toEpochMilli();
        long tillSettlementId = 0, date;
        var transactions = new JSONArray();
        Map<String, Object> params;

        while (true) {
            params = new HashMap<>() {{
                put("from", dateFrom);
                put("limit", ENTITIES_LIMIT);
            }};
            if (tillSettlementId != 0) {
                params.put("till", tillSettlementId);
            } else {
                params.put("to", dateTo);
            }

            var result = new JSONArray(this.connector.request("settlementTransactionHistory", params));
            if (result.length() == 0) {
                break;
            }
            for (int index = 0; index < result.length(); index++) {
                date = (long) ((JSONArray) result.get(index)).get(9);
                if (date < dateFrom) {
                    break;
                }
                transactions.put(result.getJSONArray(index));
                tillSettlementId = (int) ((JSONArray) result.get(index)).get(10);
            }
        }

        LOGGER.info(String.format("Settlement transaction history within last 24 hours:\n%s", transactions));
    }

    public void addOrder() throws NoSuchAlgorithmException, InvalidKeyException, JSONException, IOException, InterruptedException {
        var instrument = "BTC-USD";
        var clientOrderId = 42;
        var price = 31_010.12;
        var size = 0.001;
        var orderType = "limit";
        var side = "bid"; // or ask

        var trade = new JSONObject(this.connector.request("add", Map.of(
                "instrument", instrument,
                "clientOrderId", clientOrderId,
                "price", (long) (price * Math.pow(10, 8)),
                "size", (long) (size * Math.pow(10, 8)),
                "type", orderType,
                "side", side,
                "cod", true
        )));
        LOGGER.info(String.format("Add order response:\n%s", trade));
    }
}
