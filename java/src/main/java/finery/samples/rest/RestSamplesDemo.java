package finery.samples.rest;

public class RestSamplesDemo {
    public static void run() throws Exception {
        String key = System.getenv("KEY");
        String secret = System.getenv("SECRET");

        RestConnector connector = new RestConnector(key, secret, true);
        RestSamples demo = new RestSamples(connector);

        demo.showPositions();
        demo.showInstruments();
        demo.showLimits();
        demo.showCpLimits();
        demo.showSettlementRequests();
        demo.showSettlementTransactions();
        demo.showBook("BTC-USD", false);
        demo.showDealHistory();
        demo.showSettlementHistory();
        demo.showSettlementTransactionHistory();
        demo.addOrder();
    }
}
