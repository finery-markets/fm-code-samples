package finery.samples.websocket;

import java.util.Map;

public class WebSocketDemo {
    final private static long UNIT = (long)Math.pow(10, 8);
    public static void run() throws Exception {
        String key = System.getenv("KEY");
        String secret = System.getenv("SECRET");

        WebSocketConnector connector = new WebSocketConnector(key, secret, true);
        connector.sendMessage(Map.of("event", "bind", "feed", "P"));
        Thread.sleep(1_000);
        connector.sendMessage(Map.of("event", "bind", "feed", "B", "feedId", "BTC-USD"));
        // Wait for 5 seconds and print out global book updates for BTC-USD instrument
        Thread.sleep(5_000);
        connector.sendMessage(Map.of("event", "unbind", "feed", "P"));
        connector.sendMessage(Map.of("event", "unbind", "feed", "B", "feedId", "BTC-USD"));
        Thread.sleep(1_000);
        System.out.println("No new messages within 5 seconds.");
        Thread.sleep(5_000);
        // Add request for taker accounts, in case of maker account, please use postOnly or limit type.
        Map requestContent = Map.of("instrument", "BTC-USD", "clientOrderId", 42, "price", 45_000 * UNIT,
                "size", (long)(0.1 * UNIT), "side", "ask", "type", "limitFOK", "cod", true);
        connector.sendMessage(Map.of("event", "request", "reqId", 12345, "method", "add",
                "content", requestContent));
        Thread.sleep(1_000);
        connector.close();
    }
}
