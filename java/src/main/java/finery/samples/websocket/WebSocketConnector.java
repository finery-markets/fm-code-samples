package finery.samples.websocket;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import javax.websocket.*;


@ClientEndpoint()
public class WebSocketConnector extends Endpoint implements MessageHandler.Partial<String>, MessageHandler.Whole<String> {
    final private String key;
    final private String secret;
    final private URI host;

    private Session userSession = null;
    private MessageHandler messageHandler;
    private WebSocketContainer container;

    public WebSocketConnector(String key, String secret, Boolean isDemo) {
        try {
            this.key = key;
            this.secret = secret;
            this.host = isDemo ? new URI("wss://test.finerymarkets.com/ws") : new URI("wss://trade.finerymarkets.com/ws");
            container = ContainerProvider.getWebSocketContainer();
            userSession = container.connectToServer(this, configBuilder(), host);
            userSession.addMessageHandler(this);
        } catch (URISyntaxException | DeploymentException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        System.out.println("Connection open");
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        System.out.println("Connection Closed: " + closeReason.getReasonPhrase());
        try {
            session.close(closeReason);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onError(Session session, Throwable throwable) {
        System.out.println("Connection Failed: " + throwable.getMessage());
    }

    public void onMessage(String message) {
        System.out.println("Message: " + message);
        if (message.equals("[\"X\",0,\"Z\",0]")) {
            System.out.println("Connected to server");
        }
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(message);
        }
    }

    public void addMessageHandler(MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    public void sendMessage(String message) {
        System.out.println("Sending message: " + message);
        this.userSession.getAsyncRemote().sendText(message);
    }

    public void sendMessage(Map message) {
        sendMessage(new Gson().toJson(message));
    }

    @Override
    public void onMessage(String s, boolean b) {
        System.out.println("Message (" + (b ? "last" : "not last") + ": " + s);
    }

    public interface MessageHandler {
        void handleMessage(String message);
    }

    public ClientEndpointConfig configBuilder() {
        return ClientEndpointConfig.Builder.create().configurator(new ClientConfigurator(key, secret)).build();
    }

    public void close() {
        try {
            this.userSession.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
