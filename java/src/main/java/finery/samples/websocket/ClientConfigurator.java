package finery.samples.websocket;

import com.google.gson.Gson;
import finery.samples.utils.SigningUtils;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.HandshakeResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientConfigurator extends ClientEndpointConfig.Configurator {
    final private String key;
    final private String secret;

    public ClientConfigurator(String key, String secret) {
        this.key = key;
        this.secret = secret;
    }
    @Override
    public void beforeRequest(Map<String, List<String>> headers) {
        System.out.println("BeforeRequest");
        var newHeaders = getHeaders();
        headers.putAll(newHeaders);
        System.out.println("Before headers -> " + headers);
    }

    @Override
    public void afterResponse(HandshakeResponse hr) {
        Map<String, List<String>> headers = hr.getHeaders();
        System.out.println("After headers -> " + headers);
    }

    public Map<String, List<String>> getHeaders() {
        var content = new HashMap<>();
        long timeMilli = new Date().getTime();
        content.put("nonce", timeMilli);
        content.put("timestamp", timeMilli);

        final Gson gson = new Gson();
        String contentJson = gson.toJson(content);
        String signature = SigningUtils.getSignature(contentJson, secret);
        List<String> keyVal = new ArrayList<>();
        keyVal.add(key);
        List<String> signVal = new ArrayList<>();
        signVal.add(signature);
        List<String> contentJsonVal = new ArrayList<>();
        contentJsonVal.add(contentJson);
        return new HashMap<>(Map.of("EFX-Key", keyVal, "EFX-Sign", signVal, "EFX-Content", contentJsonVal));
    }
}
