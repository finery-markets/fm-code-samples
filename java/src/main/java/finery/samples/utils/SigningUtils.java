package finery.samples.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public final class SigningUtils {

    private SigningUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String getSignature(String value, String secret) {
        try {
            var sha384Hmac = Mac.getInstance("HmacSHA384");
            var secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.US_ASCII), "HmacSHA256");
            sha384Hmac.init(secretKey);
            return Base64.encodeBase64String(sha384Hmac.doFinal(value.getBytes(StandardCharsets.US_ASCII)));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }
}
