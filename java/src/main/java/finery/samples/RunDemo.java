package finery.samples;

import finery.samples.fix.FixSamplesDemo;
import finery.samples.rest.RestSamplesDemo;
import finery.samples.websocket.WebSocketDemo;

public class RunDemo {
    public static void main(String[] args) throws Exception {
        if (args.length == 0 || args[0].equalsIgnoreCase("rest")) {
            RestSamplesDemo.run();
        } else if (args[0].equalsIgnoreCase("websocket")) {
            WebSocketDemo.run();
        } else if (args[0].equalsIgnoreCase("fix")) {
            FixSamplesDemo.run();
        } else {
            System.out.println("Please, specify 'rest', 'websocket' or 'fix' parameter to run Demo.");
        }
    }
}
