# java-samples

Code examples Finery Markets API usage for REST, Websocket and FIX.
Please note that the given examples do not contain error handling and reconnecting logic.
Access to FIX API is provided using IP-address whitelist, which is set during the onboarding process.
Before using FIX samples you should also add API host/port info and API key in `fix.cfg` file.

To run samples need to:

1. build project

```mvn clean install ```

2. Run command below

```
export EFX_KEY=<KEY>; export EFX_SECRET=<SECRET>;
mvn exec:java -Dexec.mainClass="finery.samples.RunDemo" -Dexec.args="websocket" -Dexec.cleanupDaemonThreads=false
```

where

- <KEY> and <SECRET> are API keys that are gotten from Finery.
- '-Dexec.args' can have 3 values 'rest', 'websocket' or 'fix'.
