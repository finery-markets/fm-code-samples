# fm-code-samples

Code examples Finery Markets API usage. Please note that the given examples do not contain error handling and reconnecting logic.

The given examples are in implemented in:
 * Python 3
 * Golang
 * NodeJS
 * Java

To run these examples generate API keys for your account and set environment variables EFX_KEY and EFX_SECRET or
substitute corresponding variables in the code.
